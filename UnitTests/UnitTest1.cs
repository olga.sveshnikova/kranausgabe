﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Logik;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class UnitTestHacken
    {
        [TestMethod]
        public void HakenRunterBisFundament()
        {
            //Arrange
            Kran kran = new Kran();
            Panel haken = new Panel();
            haken.Size = new Size(10, 10);
            haken.Location = new Point(10, 10);

            Panel fundament = new Panel();
            fundament.Location = new Point(30, 30);

            //Act
            kran.HakenRunter(haken, fundament);
            //Assert
            Assert.AreNotEqual(haken.Height + haken.Location.Y, fundament.Location.Y);
        }
    }

    [TestClass]
    public class UnitTestAusleger
    {
        [TestMethod]
        public void AuslegerEinBisMast()
        {
            //Arrange
            Kran kran = new Kran();
            Panel haken = new Panel();
            haken.Size = new Size(10, 10);
            haken.Location = new Point(10, 20);
            Panel ausleger = new Panel();
            ausleger.Size = new Size(10, 10);
            ausleger.Location = new Point(10, 10);
            Panel mast = new Panel();
            mast.Location = new Point(30, 10);


            //Act
            kran.AuslegerEin(haken, ausleger, mast);
            //Assert
            Assert.AreNotEqual(ausleger.Location.X, mast.Location.X);
            Assert.AreNotEqual(haken.Location.X + haken.Width, mast.Location.X);
        }

        [TestMethod]
        public void AuslegerAusBisBildGrenze()
        {
            //Arrange
            Kran kran = new Kran();
            Panel haken = new Panel();
            haken.Location = new Point(10, 10);
            Panel ausleger = new Panel();
            ausleger.Location = new Point(10, 10);

            //Act
            kran.AuslegerAus(haken, ausleger);

            //Assert
            Assert.AreEqual(0, ausleger.Location.X);
        }
    }

    [TestClass]
    public class UnitTestKran
    {
        [TestMethod]
        public void KranLinksBisGrenze()
        {
            Kran kran = new Kran();
            
            Panel haken = new Panel();
            haken.Size = new Size(10, 10);
            haken.Location = new Point(10, 20);
            Panel ausleger = new Panel();
            ausleger.Size = new Size(10, 10);
            ausleger.Location = new Point(10, 10);
            Panel mast = new Panel();
            mast.Location = new Point(30, 10);
            Panel fundament = new Panel();
            fundament.Location = new Point(20, 40);


            kran.KranLinks(haken, ausleger,mast,fundament);

            //Assert
            Assert.AreEqual(0, ausleger.Location.X);
            Assert.AreEqual(0, haken.Location.X);
        }

        [TestMethod]
        public void KranRechtsBisButtons()
        {
            Kran kran = new Kran();

            Panel haken = new Panel();
            haken.Size = new Size(10, 10);
            haken.Location = new Point(10, 20);
            Panel ausleger = new Panel();
            ausleger.Size = new Size(10, 10);
            ausleger.Location = new Point(20, 10);
            Panel mast = new Panel();
            mast.Location = new Point(20, 10);
            mast.Size = new Size(10, 20);
            Panel fundament = new Panel();
            fundament.Location = new Point(20, 40);
            fundament.Size = new Size(10, 10);
            Button button = new Button();
            button.Location = new Point(40, 10);


            kran.KranRechts(haken, ausleger, mast, fundament, button);

            //Assert
            Assert.AreEqual(button.Location.X, ausleger.Location.X+ausleger.Width);
            Assert.AreEqual(button.Location.X, fundament.Location.X + fundament.Width);
            Assert.AreEqual(button.Location.X, mast.Location.X + mast.Width);
        }

        [TestMethod]
        public void KranAusBisGrenze()
        {
            Kran kran = new Kran();

            Panel haken = new Panel();
            haken.Size = new Size(10, 10);
            haken.Location = new Point(10, 10);
            Panel ausleger = new Panel();
            ausleger.Size = new Size(10, 10);
            ausleger.Location = new Point(10, 10);
            Panel mast = new Panel();
            mast.Location = new Point(30, 10);
            Panel fundament = new Panel();
            fundament.Location = new Point(20, 40);


            kran.KranAus(haken, ausleger, mast);

            //Assert
            Assert.AreEqual(0, ausleger.Location.Y);
            Assert.AreEqual(0, haken.Location.Y);
        }
        
        [TestMethod]
        public void KranEinBisFundament()
        {
            Kran kran = new Kran();

            Panel haken = new Panel();
            haken.Size = new Size(10, 10);
            haken.Location = new Point(10, 20);
            Panel ausleger = new Panel();
            ausleger.Size = new Size(10, 20);
            ausleger.Location = new Point(10, 10);
            Panel mast = new Panel();
            mast.Location = new Point(30, 10);
            Panel fundament = new Panel();
            fundament.Location = new Point(20, 40);


            kran.KranEin(haken, ausleger, mast, fundament);

            //Assert
            Assert.AreEqual(fundament.Location.Y, ausleger.Location.Y+ausleger.Height);
            Assert.AreEqual(fundament.Location.Y, haken.Location.Y+haken.Height);
        }

    }
}
