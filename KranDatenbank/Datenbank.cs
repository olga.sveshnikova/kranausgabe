﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KranDatenbank
{
    public class Datenbank
    {
        MySqlConnection connection;
        public Datenbank()
        {
            connection = GetConnection();
            connection.Open();
        }

        public void ErstelleEinDatenBank()
        {
            //CREATE DATABASE `kran_db`
            //CREATE TABLE `parameter` (`id` INT ,`x` INT , `y` INT , `breite` INT ,`hoehe` INT);
            //CREATE TABLE `bauteile` (`id` INT NOT NULL, `bezeichner` VARCHAR(50) NOT NULL DEFAULT '', PRIMARY KEY(`id`));
        }
        public MySqlConnection GetConnection()
        {
           try {new MySqlConnection("Server=localhost;Database=kran_db;Uid=root;");
            }
           catch (Exception e)
            {
                ErstelleEinDatenBank();
            }
        
           return new MySqlConnection("Server=localhost;Database=kran_db;Uid=root;");
        }

        public void FundamentParameter(int x, int y, int breite, int hoehe)
        {
            ParameterUbertragen(1, x, y, breite, hoehe);
        }

        public void MastParameter(int x, int y, int breite, int hoehe)
        {
            ParameterUbertragen(2, x, y, breite, hoehe);
        }

        public void AuslegerParameter(int x, int y, int breite, int hoehe)
        {
            ParameterUbertragen(3, x, y, breite, hoehe);
        }

        public void HakenParameter(int x, int y, int breite, int hoehe)
        {
            ParameterUbertragen(4, x, y, breite, hoehe);
        }

        public void ParameterUbertragen(int id, int x, int y, int breite, int hoehe) {
            MySqlCommand behehl = new MySqlCommand("UPDATE parameter SET x =" + x + ", y = " + y + ", breite = " + breite + ", hoehe=" + hoehe + " WHERE id = " + id + ";", connection);
            behehl.ExecuteNonQuery();
        }

        public int[] FundamentParameter()
        {
            return ParameterBekommen(1);
        }
        public int[] MastParameter()
        {
            return ParameterBekommen(2);
        }

        public int[] AuslegerParameter()
        {
            return ParameterBekommen(3);
        }

        public int[] HakenParameter()
        {
            return ParameterBekommen(4);
        }

        public int[] ParameterBekommen(int id) {
           MySqlCommand sqlBefehl = new MySqlCommand("Select * from parameter where id ="+id+";", connection);      
           MySqlDataReader reader = sqlBefehl.ExecuteReader();
            int[] parameter = new int[4];
            while (reader.Read())
            {
               parameter[0] = reader.GetInt32("x");
               parameter[1] = reader.GetInt32("y");
               parameter[2] = reader.GetInt32("breite");
               parameter[3] = reader.GetInt32("hoehe");
            }
            reader.Close();
            return parameter;
        }

        public void CloseConnection()
        {
            connection.Close();
        }
    }
}

