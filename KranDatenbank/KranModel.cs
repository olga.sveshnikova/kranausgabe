﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KranDatenbank
{
    public class KranModel
    {

    }

    public class Bauteil
    {
        int id { get; set; }
        int x { get; set; }
        int y { get; set; }
        int breite { get; set; }
        int hoehe { get; set; }

        public Bauteil(int id, int locationX, int locationY, int breite, int hoehe){
            this.id = id;
            this.x = locationX;
            this.y = locationY;
            this.breite = breite;
            this.hoehe = hoehe;
        }


    }
}
