﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Logik
{
    public class Kran
    {
        int standartSchritt = 10; 

        public void HakenRunter (Panel haken, Panel fundament)
        {
            if (haken.Size.Height + haken.Location.Y + standartSchritt < fundament.Location.Y)
            {
                haken.Height += standartSchritt;
            }
            
        }

        public void HakenHoch(Panel haken)
        {
            haken.Height -= standartSchritt;
        }

        public void AuslegerAus(Panel haken, Panel ausleger) {

            if (ausleger.Location.X >= standartSchritt)
            {
                ausleger.Location = new Point(ausleger.Location.X - standartSchritt, ausleger.Location.Y);
                ausleger.Width += standartSchritt;
                haken.Location = new Point(haken.Location.X - standartSchritt, haken.Location.Y);
            }

        }

        public void AuslegerEin(Panel haken, Panel ausleger, Panel mast)
        {
            if (haken.Location.X + haken.Width + 10 < mast.Location.X)
            {
                ausleger.Location = new Point(ausleger.Location.X + standartSchritt, ausleger.Location.Y);
                ausleger.Width -= standartSchritt;
                haken.Location = new Point(haken.Location.X + standartSchritt, haken.Location.Y);
            }

        }

        public void KranLinks (Panel haken, Panel ausleger, Panel mast, Panel fundament)
        {
            if (ausleger.Location.X >= standartSchritt) { 
            haken.Location = new Point(haken.Location.X - standartSchritt, haken.Location.Y);
            ausleger.Location = new Point(ausleger.Location.X - standartSchritt, ausleger.Location.Y);
            mast.Location = new Point(mast.Location.X - standartSchritt, mast.Location.Y);
            fundament.Location = new Point(fundament.Location.X - standartSchritt, fundament.Location.Y);
            }

        }

        public void KranRechts(Panel haken, Panel ausleger, Panel mast, Panel fundament, Button button1)
        {
            if (mast.Location.X + mast.Width < button1.Location.X)
            { 
            haken.Location = new Point(haken.Location.X + standartSchritt, haken.Location.Y);
            ausleger.Location = new Point(ausleger.Location.X + standartSchritt, ausleger.Location.Y);
            mast.Location = new Point(mast.Location.X + standartSchritt, mast.Location.Y);
            fundament.Location = new Point(fundament.Location.X + standartSchritt, fundament.Location.Y);
            }
        }

        public void KranAus(Panel haken, Panel ausleger, Panel mast)
        {
            if (ausleger.Location.Y-standartSchritt >= 0)
            {
                haken.Location = new Point(haken.Location.X, haken.Location.Y - standartSchritt);
                ausleger.Location = new Point(ausleger.Location.X, ausleger.Location.Y - standartSchritt);
                mast.Location = new Point(mast.Location.X, mast.Location.Y- standartSchritt);
                mast.Height += standartSchritt;
            }

        }

        public void KranEin(Panel haken, Panel ausleger, Panel mast, Panel fundament)
        {
            if (haken.Location.Y + haken.Height < fundament.Location.Y)
            {
                haken.Location = new Point(haken.Location.X, haken.Location.Y + standartSchritt);
                ausleger.Location = new Point(ausleger.Location.X, ausleger.Location.Y + standartSchritt);
                mast.Location = new Point(mast.Location.X, mast.Location.Y + standartSchritt);
                mast.Height -= standartSchritt;
            }
        }
    }
}
