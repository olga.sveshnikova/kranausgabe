﻿namespace KranAufgabe
{
    partial class Abbildung
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.fundament = new System.Windows.Forms.Panel();
            this.mast = new System.Windows.Forms.Panel();
            this.ausleger = new System.Windows.Forms.Panel();
            this.haken = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.last_Configuration = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(661, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Haken hoch";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.HakenHoch_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(661, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Haken runter";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.HakenRunter_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(661, 128);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(127, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Ausleger ein";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.AuslegerEin_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(661, 99);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(127, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Ausleger aus";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.AuslegerAus_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(661, 157);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(127, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Kran rechts";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.KranRechts_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(661, 186);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(127, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "Kran links";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.KranLinks_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(661, 215);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(127, 23);
            this.button7.TabIndex = 6;
            this.button7.Text = "Kran aus";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.KranAus_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(661, 244);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(127, 23);
            this.button8.TabIndex = 7;
            this.button8.Text = "Kran ein";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.KranEin_Click);
            // 
            // fundament
            // 
            this.fundament.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.fundament.Location = new System.Drawing.Point(426, 324);
            this.fundament.Name = "fundament";
            this.fundament.Size = new System.Drawing.Size(159, 37);
            this.fundament.TabIndex = 8;
            // 
            // mast
            // 
            this.mast.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.mast.Location = new System.Drawing.Point(483, 93);
            this.mast.Name = "mast";
            this.mast.Size = new System.Drawing.Size(36, 234);
            this.mast.TabIndex = 9;
            // 
            // ausleger
            // 
            this.ausleger.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ausleger.Location = new System.Drawing.Point(319, 64);
            this.ausleger.Name = "ausleger";
            this.ausleger.Size = new System.Drawing.Size(200, 29);
            this.ausleger.TabIndex = 10;
            // 
            // haken
            // 
            this.haken.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.haken.Location = new System.Drawing.Point(319, 93);
            this.haken.Name = "haken";
            this.haken.Size = new System.Drawing.Size(29, 39);
            this.haken.TabIndex = 11;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(661, 324);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(138, 23);
            this.button9.TabIndex = 12;
            this.button9.Text = "speichern";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Speichern_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(661, 389);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(138, 23);
            this.button10.TabIndex = 13;
            this.button10.Text = "zurücksetzen";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.reset_Click);
            // 
            // last_Configuration
            // 
            this.last_Configuration.Location = new System.Drawing.Point(661, 360);
            this.last_Configuration.Name = "last_Configuration";
            this.last_Configuration.Size = new System.Drawing.Size(138, 23);
            this.last_Configuration.TabIndex = 14;
            this.last_Configuration.Text = "letzte Konfiguration";
            this.last_Configuration.UseVisualStyleBackColor = true;
            this.last_Configuration.Click += new System.EventHandler(this.last_Configuration_Click);
            // 
            // Abbildung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.last_Configuration);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.haken);
            this.Controls.Add(this.ausleger);
            this.Controls.Add(this.mast);
            this.Controls.Add(this.fundament);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Abbildung";
            this.Text = "Kran";
            this.Load += new System.EventHandler(this.Speichern_Click);
            this.Click += new System.EventHandler(this.Speichern_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Panel fundament;
        private System.Windows.Forms.Panel mast;
        private System.Windows.Forms.Panel ausleger;
        private System.Windows.Forms.Panel haken;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button last_Configuration;
    }
}

