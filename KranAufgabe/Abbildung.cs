﻿using KranDatenbank;
using Logik;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KranAufgabe
{
    public partial class Abbildung : Form
    {
        Kran kran = new Kran();
        public Abbildung()
        {
            InitializeComponent();
        }

        private void HakenHoch_Click(object sender, EventArgs e)
        {
            kran.HakenHoch(haken);
        }

        private void HakenRunter_Click(object sender, EventArgs e)
        {
            kran.HakenRunter(haken, fundament);
        }

        private void AuslegerAus_Click(object sender, EventArgs e)
        {
            kran.AuslegerAus (haken, ausleger);
        }

        private void AuslegerEin_Click(object sender, EventArgs e)
        {
            kran.AuslegerEin(haken, ausleger, mast);
        }

        private void KranLinks_Click(object sender, EventArgs e)
        {
            kran.KranLinks(haken, ausleger, mast,fundament);
        }

        private void KranRechts_Click(object sender, EventArgs e)
        {
            kran.KranRechts(haken, ausleger, mast, fundament, button1);
        }

        private void KranEin_Click(object sender, EventArgs e)
        {
            kran.KranEin(haken, ausleger, mast, fundament);
        }

        private void KranAus_Click(object sender, EventArgs e)
        {
            kran.KranAus(haken, ausleger, mast);
        }

        private void Speichern_Click(object sender, EventArgs e)
        {
            Datenbank datenbank = new Datenbank();
            datenbank.FundamentParameter(fundament.Location.X, fundament.Location.Y, fundament.Width, fundament.Height);
            datenbank.MastParameter(mast.Location.X, mast.Location.Y, mast.Width, mast.Height);
            datenbank.AuslegerParameter(ausleger.Location.X, ausleger.Location.Y, ausleger.Width, ausleger.Height);
            datenbank.HakenParameter(haken.Location.X, haken.Location.Y, haken.Width, haken.Height);
            datenbank.CloseConnection();
        }

        private void reset_Click(object sender, EventArgs e)
        {
            // fundament         
            this.fundament.Location = new Point(426, 324);
            this.fundament.Size = new Size(159, 37);
            
            // mast             
            this.mast.Location = new Point(483, 93);
            this.mast.Size = new Size(36, 234);
            
            // ausleger           
            this.ausleger.Location = new Point(319, 64);
            this.ausleger.Size = new Size(200, 29);
            
            // haken
            this.haken.Location = new Point(319, 93);
            this.haken.Size = new Size(29, 39);
            
        }


        private void last_Configuration_Click(object sender, EventArgs e)
        {
            Datenbank datenbank = new Datenbank();
            int[] fp = datenbank.FundamentParameter();
            this.fundament.Location = new Point(fp[0], fp[1]);
            this.fundament.Size = new Size(fp[2], fp[3]);

            int[] mp = datenbank.MastParameter();
            this.mast.Location = new Point(mp[0], mp[1]);
            this.mast.Size = new Size(mp[2], mp[3]);

            int[] ap = datenbank.AuslegerParameter();
            this.ausleger.Location = new Point(ap[0], ap[1]);
            this.ausleger.Size = new Size(ap[2], ap[3]);

            int[] hp = datenbank.HakenParameter();
            this.haken.Location = new Point(hp[0], hp[1]);
            this.haken.Size = new Size(hp[2], hp[3]);

            datenbank.CloseConnection();
        }      
    }
}
